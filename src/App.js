import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route } from 'react-router-dom';
import LoginMenu from './components/auth/Login/Login';
import RegisterMenu from './components/auth/Register/Register';
import DaftarJualMenu from './components/DaftarJual';
import InfoProfil from './components/info-profil';
import InfoProduk from './components/info-produk';
import HomePage from './components/HomePage';
import ProductPageSeller from './components/Seller/ProductPage';
import ProductPageBuyer from './components/Buyer';
import InfoPenawarMenu from './components/InfoPenawar';

import './App.css';
import UpdateProduct from './components/info-produk/UpdateProduct';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/login" element={<LoginMenu />} />
        <Route path="/register" element={<RegisterMenu />} />
        <Route path="/daftarjual" element={<DaftarJualMenu />} />
        <Route path="/info-profil" element={<InfoProfil />} />
        <Route path="/info-produk" element={<InfoProduk />} />
        <Route path="/info-produk/:id" element={<UpdateProduct />} />
        <Route path="/" element={<HomePage />} />
        <Route path="/info-penawar/:id" element={<InfoPenawarMenu />} />
        <Route path="/seller-product/:id" element={<ProductPageSeller />} />
        <Route path="/product/details/:id" element={<ProductPageBuyer />} />
      </Routes>
    </div>
  );
}

export default App;
