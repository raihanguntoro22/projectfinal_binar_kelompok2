import React from 'react';

const Select = ({label, ...rest}) => {
    return (
        <div className="form-group mb-2">
        <p className='label'>{label}</p>
        <select required className="form-select mt-2" {...rest}>
            <option value="" disabled selected hidden>Pilih Kota</option>
            <option>Semarang</option>
            <option>Surabaya</option>
            <option>Malang</option>
            <option>Banda Aceh</option>
            <option>Bandung</option>
            <option>Jakarta</option>
        </select>
    </div>
    )
}

export default Select