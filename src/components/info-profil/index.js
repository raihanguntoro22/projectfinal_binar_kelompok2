import { useState, useEffect, useRef } from 'react'
import axios from 'axios'
import { useNavigate, useParams } from "react-router-dom";
import Input from "./Input"
import Select from "./Select";
import Button from "./Button";
import Upload from "./Upload";

const client = axios.create({
	baseURL: `https://finalproject-be-kelompok2.herokuapp.com/user/details`,
  headers: {
    Authorization: `Bearer ${localStorage.getItem("token")}`,
    "Content-Type" : "application/json",
  },
  validateStatus: (status) => {
    return true; // I'm always returning true, you may want to do it depending on the status received
  },
});

const UpdateInfoProfil = () => {
    const navigate = useNavigate();

    //const [image, setImage] = useState('')
    const [image, setImage] = useState({
      myFile: "",
    });
    const [name, setName] = useState('')
    const [city, setCity] = useState('')
    const [address, setAddress] = useState('')
    const [phone, setPhone] = useState('')
    const [imagePreview, setImagePreview] = useState('picture.png');
    const [posts, setPosts] = useState([]);

  // Handle form submission
  const handleSubmit = (e) => {
  e.preventDefault();
  addPosts(image, name, city, address, phone);
        console.log('image: ', image)
        console.log('name: ', name)
        console.log('city: ', city)
        console.log('address: ', address)
        console.log('phone: ', phone)
  };
    /* POST with Axios */
	const addPosts = async (image, name, city, address, phone) => {
		try {
			let response = await client.post('',  {
				image: image,
				name: name,
        city: city,
        address, address,
        phone: phone,
			});
			setPosts([response.data, ...posts]);
      setImage('');
			setName('');
      setCity('');
      setAddress('');
      setPhone('');
		} catch (error) {
			console.log(error.response.data);
		}
	};

  const convertToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

    const onImageUpload = async (e) => {
    const file = e.target.files[0];
    setImagePreview(URL.createObjectURL(file))
    const base64 = await convertToBase64(file);
    setImage({ ...image, myFile: base64 });
    }

    return (
        <div className=''>
            <nav className="navbar navbar-expand-lg nav-info-profil">
            <div class="container-fluid">
                <a style={{fontFamily: 'Poppins'}} className="navbar-brand navbar-info-profil" href="#">
                 <img className="rectangle-navbar" style={{marginRight: 285}} src="rectangle.png" width={150} height={24} alt />
                 Lengkapi Info Akun
                </a>
            </div>
            </nav>
            <div className='form'>
            <form onSubmit={handleSubmit}>
            <div className="container mb-2 mt-4">
             <div className="row">
                <div className="col">
                <a onClick={() => navigate("/daftarjual")}>
                <img className='left-arrow' style={{marginRight: 0}} src="left-arrow.png" alt />
                </a>
                </div>
                <Upload onChange={(e) => onImageUpload(e)} image={imagePreview}></Upload>
              </div>
            </div>
            <Input label="Nama*" value={name} name="name" onChange={(e) => setName(e.target.value)} placeholder='Nama'></Input>
            <Select label="Kota*" value={city} name="city"  onChange={(e) => setCity(e.target.value)}></Select>
            <Input label="Alamat*" value={address} name="address"  onChange={(e) => setAddress(e.target.value)} placeholder='Contoh: Jalan Ikan Hiu 33'></Input>
            <Input label="No Handphone*" value={phone} name="phone"  onChange={(e) => setPhone(e.target.value)} placeholder='contoh: +628123456789'></Input>
            <br />
            <Button title="Simpan" type="submit"></Button>
            </form>
            </div>
        </div>
    )
}

export default UpdateInfoProfil