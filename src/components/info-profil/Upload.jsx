import React from 'react';

const Upload = ({image, ...rest}) => {
    return (
        <div className="col upload">
            <label for="img">{image && <img for="img" className='preview' src={image} alt="preview" />}</label>
            <input required name="myFile" accept=".jpeg, .png, .jpg" id="img" className='picture-camera' type="file" {...rest}/>
        </div>
    )
}

export default Upload