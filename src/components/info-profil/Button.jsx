import React from 'react';

const Button = ({title, ...rest}) => {
    return (
        <button type="submit" className="btn btn-simpan button-purple" {...rest}>{title}</button>
    )
}

export default Button