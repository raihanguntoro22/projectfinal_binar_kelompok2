import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { UncontrolledAlert } from 'reactstrap';
import CardUser from './CardUser';
import ModalTawar from './ModalTawar';

function CardProduct({ detailProduct, detailUser }) {
  const { id } = useParams();
  const [modal, setModal] = useState(false);
  const [validasi, setValidasi] = useState(false);
  const toggleModal = () => setModal(!modal);

  const navigate = useNavigate();
  const [price, setPrice] = useState(null);
  const [status, setStatus] = useState([]);

  const addTawar = async () => {
    console.log('price: ', price);
    let formField = {
      productId: detailProduct.id,
      price: price,
    };
    const token = localStorage.getItem('token');

    await axios({
      method: 'post',
      url: 'https://finalproject-be-kelompok2.herokuapp.com/offer/create',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: formField,
    })
      .then((response) => {
        console.log(response);
      })
      .then(() => {
        setValidasi(true);
        const timer = setTimeout(() => {
          setValidasi(false);
        }, 3000);

        return () => clearTimeout(timer);
      });
  };
  useEffect(() => addTawar, []);

  const getProductsData = async () => {
    try {
      const response = await axios.get(`https://finalproject-be-kelompok2.herokuapp.com/offer/${id}`, {
        headers: { authorization: localStorage.getItem('token') },
      });
      setStatus(response.data.data.status);
      const response2 = await axios.get(
        `https://finalproject-be-kelompok2.herokuapp.com/order/${response.data.data.id}`
      );
      setStatus(response2.data.data.status);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => getProductsData, []);
  console.log('ini offer', id);
  console.log('status ini', status);

  return (
    <div>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{detailProduct.name}</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            {detailProduct.categoryId}
          </h6>
          <p className="card-text">Rp. {detailProduct.price}</p>
          {status === 'pending' ? (
            <button
              className="card-link btn btn-terbit w-100 mb-3"
              onClick={toggleModal}
              style={{ backgroundColor: '#D0D0D0' }}
            >
              Menunggu response penjual
            </button>
          ) : status === 'rejected' ? (
            <button
              className="card-link btn btn-terbit w-100 mb-3"
              onClick={toggleModal}
            >
              Silahkan update harga tawar mu
            </button>
          ) : status === 'accepted' ? (
            <button
              className="card-link btn btn-terbit w-100 mb-3"
              onClick={toggleModal}
              style={{ backgroundColor: '#D0D0D0' }}
              disabled
            >
              Product ini sudah terjual
            </button>
          ) : (
            <button
              className="card-link btn btn-terbit w-100 mb-3"
              onClick={toggleModal}
            >
              Saya Tertarik dan ingin Nego
            </button>
          )}
        </div>
      </div>
      <CardUser detailProduct={detailProduct} detailUser={detailUser} />
      {/* modal tawar */}
      <ModalTawar
        modal={modal}
        toggleModal={toggleModal}
        detailProduct={detailProduct}
        detailUser={detailUser}
        price={price}
        setPrice={setPrice}
        addTawar={addTawar}
      />
      {/* validasi kirim */}
      {validasi && (
        <UncontrolledAlert className="bg-success alert-tawar">
          Harga tawarmu berhasil dikirim ke penjual
        </UncontrolledAlert>
      )}
    </div>
  );
}
export default CardProduct;
