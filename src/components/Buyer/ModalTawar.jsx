import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
function ModalTawar({
  modal,
  toggleModal,
  detailUser,
  price,
  setPrice,
  addTawar,
}) {
  return (
    <div>
      <Modal centered isOpen={modal} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal} className="me-4 ms-4">
          Masukkan Harga Tawarmu
        </ModalHeader>
        <ModalBody className="me-4 ms-4">
          Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan
          segera dihubungi penjual.
          <div className="card mt-3 card-profile">
            <div className="row g-0">
              <div className="col-md-4 me-3">
                <img
                  src="img/img-product.png"
                  className="img-fluid rounded-start p-3"
                  alt="..."
                />
              </div>
              <div className="col-md-7">
                <div
                  className="card-body"
                  style={{ backgroundColor: '#EEEEEE' }}
                >
                  <p className="card-text">{detailUser.name}</p>
                  <p className="card-text">
                    <small className="text-muted">{detailUser.address}</small>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">
              Harga tawar
            </label>
            <input
              type="text"
              class="form-control"
              placeholder="Rp. 0,00"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
        </ModalBody>

        <ModalFooter>
          <Button
            className="w-100 btn-purple ms-5 me-5"
            color="primary"
            toggle={toggleModal}
            onClick={addTawar}
          >
            Kirim
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}
export default ModalTawar;
