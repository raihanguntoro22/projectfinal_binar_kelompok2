import { UncontrolledPopover, PopoverBody } from 'reactstrap';
function NotifikasiTawar() {
  return (
    <div>
      <UncontrolledPopover
        placement="bottom"
        target="PopoverClick"
        trigger="click"
      >
        <PopoverBody>
          <div className=" card-notifikasi">
            <div className="row g-0">
              <div className="col-md-2 me-2 ">
                <img
                  src="img/img-product.png"
                  className="img-fluid rounded-start"
                  alt="..."
                />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <small className="text-muted">
                    <span className="tawar"> Penawaran produk</span>
                    <span>20 Apr,14:04</span>
                  </small>
                  <p className="card-text">Jam Tangan Casio</p>
                  <p className="card-text">Rp 250.000</p>
                  <p className="card-text">Berhasil Ditawar Rp 200.000</p>
                  <p className="card-text">
                    <small className="text-muted">
                      Kamu akan segera dihubungi penjual via whatsap
                    </small>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </PopoverBody>
      </UncontrolledPopover>
    </div>
  );
}

export default NotifikasiTawar;
