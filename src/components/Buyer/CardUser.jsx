function CardUser({ detailUser }) {
  return (
    <div>
      <div className="card mt-3 card-user">
        <div className="row g-0">
          <div className="col-md-4 me-3">
            <img
              src={detailUser.image}
              className="img-fluid rounded-start"
              alt="..."
            />
          </div>
          <div className="col-md-7">
            <div className="card-body">
              <p className="card-text">{detailUser.name}</p>
              <p className="card-text">
                <small className="text-muted">{detailUser.address}</small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardUser;
