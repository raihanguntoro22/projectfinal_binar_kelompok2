import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Navbar,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  Form,
  Input,
  NavLink,
} from 'reactstrap';
import { AiOutlineHome, AiOutlineUnorderedList } from 'react-icons/ai';
import { FiUser } from 'react-icons/fi';
import { IoIosNotificationsOutline } from 'react-icons/io';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';


function NavbarProduct() {
  const { id } = useParams();
  const [modalLogout, setModalLogout] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  function handleModalLogout() {
    setModalLogout(!modalLogout);
  }

  const [notifikasi, setNotifikasi] = useState([]);
  const getProductsData = async () => {
    try {
      const response = await axios.get(`https://finalproject-be-kelompok2.herokuapp.com/order/${id}`, {
        headers: { authorization: localStorage.getItem('token') },
      });
      setNotifikasi(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => getProductsData, []);

  return (
    <div>
      <Navbar
        expand="lg"
        light
        container
        className="shadow-sm mb-3 navbar-seller"
      >
        <Link className="navbarBrand" to="/"></Link>
        <NavbarToggler
          onClick={() => {
            setIsOpen(!isOpen);
          }}
        />
        <Collapse isOpen={isOpen} navbar>
          <Form className="me-auto">
            <Input
              id="exampleSearch"
              name="search"
              placeholder="Cari di sini...."
              type="search"
            />
          </Form>
          <Nav className="ms-auto" navbar>
            <NavItem>
              <NavLink href="#">
                <AiOutlineUnorderedList />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#" id="PopoverClick" onClick={handleModalLogout}>
                <IoIosNotificationsOutline />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">
                <Link to="/info-profil">
                  <FiUser />
                </Link>
              </NavLink>
            </NavItem>
            <Link
              to="/"
              className="btn btn-purple border-radius ms-2 btn-logout"
            >
              <AiOutlineHome />
            </Link>
          </Nav>
        </Collapse>
      </Navbar>
      {modalLogout && (
        <div
          className="card-atas position-absolute  "
          style={{
            width: '400px',
            right: '70px',
            top: '60px',
          }}
        >
          <div className=" card-notifikasi">
            <div className="row g-0">
              <div className="col-md-2 me-2 ">
                <img
                  src="img/img-product.png"
                  className="img-fluid rounded-start"
                  alt="..."
                />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <small className="text-muted">
                    <span className="tawar"> Penawaran produk</span>
                    <span>20 Apr,14:04</span>
                  </small>
                  <p className="card-text">Jam Tangan Casio</p>
                  <p className="card-text">Rp 250.000</p>
                  <p className="card-text">Berhasil Ditawar Rp 200.000</p>
                  <p className="card-text">
                    <small className="text-muted">
                      Kamu akan segera dihubungi penjual via whatsap
                    </small>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default NavbarProduct;
