import { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import Input from './Input';
import Select from './Select';
import ButtonPreview from './ButtonPreview';
import ButtonTerbitkan from './ButtonTerbitkan';
import Upload from './Upload';

const UpdateProduct = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const [name, setName] = useState(null);
  const [price, setPrice] = useState(null);
  const [categoryId, setCategoryId] = useState(null);
  const [description, setDescription] = useState(null);
  const [quantity, setQuantity] = useState('');
  const [image, setImage] = useState({
    myFile: '',
  });
  const [imagePreview, setImagePreview] = useState('input-image.png');

  useEffect(() => {
    loadInfoProduct();
  }, []);

  // load info produk by its is and show data to forms by value

  let loadInfoProduct = async () => {
    const result = await axios.get(
      `https://finalproject-be-kelompok2.herokuapp.com/product/details/${id}`
    );

    console.log(result.data.data.name);

    setName(result.data.data.name);
    setPrice(result.data.data.price);
    setCategoryId(result.data.data.categoryId);
    setDescription(result.data.data.description);
    setQuantity(result.data.data.quantity);
    setImage(result.data.data.image);
  };

  // Update s single info product

  const updateInfoProduct = async () => {
    console.log('name: ', name);
    console.log('price: ', price);
    console.log('category: ', categoryId);
    console.log('description: ', description);
    console.log('description: ', quantity);
    console.log('image: ', image);

    let formField = {
      name: name,
      price: price,
      categoryId: categoryId,
      description: description,
      quantity: quantity,
      image: image,
      id: id,
    };
    const token = localStorage.getItem('token');

    await axios({
      method: 'put',
      url: 'https://finalproject-be-kelompok2.herokuapp.com/product/update',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: formField,
    }).then((response) => {
      console.log(response.data);
      navigate('/daftarjual');
      alert('Successfully Adding Info Product !');
    });
  };

  const convertToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  const onImageUpload = async (e) => {
    const file = e.target.files[0];
    setImagePreview(URL.createObjectURL(file));
    const base64 = await convertToBase64(file);
    setImage({ ...image, myFile: base64 });
  };

  return (
    <div className="">
      <nav className="navbar navbar-expand-lg nav-info-produk">
        <div class="container-fluid">
          <a
            style={{ fontFamily: 'Poppins' }}
            className="navbar-brand"
            href="#"
          >
            <img
              className="rectangle-navbar"
              style={{ marginRight: 335 }}
              src="rectangle.png"
              width={150}
              height={24}
              alt
            />
          </a>
        </div>
      </nav>
      <div className="form">
        <div className="container mb-2">
          <div className="row">
            <div className="col">
              <a onClick={() => navigate('/')}>
                <img className="left-arrow-1" src="left-arrow.png" alt />
              </a>
            </div>
          </div>
        </div>
        <div className="form-seller">
          <Input
            label="Nama Produk"
            name="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Nama Produk"
          ></Input>
          <Input
            label="Harga Produk"
            name="price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            placeholder="Rp 0,00"
          ></Input>
          <Select
            label="Kategori"
            name="categoryId"
            value={categoryId}
            onChange={(e) => setCategoryId(e.target.value)}
          ></Select>
          <Input
            label="Deskripsi"
            name="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Contoh: Jalan Ikan Hiu 33"
            style={{ height: 80, marginBottom: 9 }}
          ></Input>
          <Input
            label="Quantity"
            name="quantity"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
            placeholder="Contoh: 3"
          ></Input>
          <Upload
            label="Foto Produk"
            onChange={(e) => onImageUpload(e)}
            img={imagePreview}
          ></Upload>
        </div>
        <br />
        <ButtonPreview
          title="Preview"
          onClick={() => navigate('/seller-product/:id')}
        ></ButtonPreview>

        <ButtonTerbitkan
          title="Terbitakan"
          onClick={updateInfoProduct}
        ></ButtonTerbitkan>
      </div>
    </div>
  );
};

// TODO fungsi onClik button terbitkan berubah tergantung nilai result (baris 30), kalau result nggak ada berarti addproduct, kalo result ada berarti updateproduct
export default UpdateProduct;
