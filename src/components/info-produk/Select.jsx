import React from 'react';

const Select = ({label, ...rest}) => {
    return (
        <div className="form-group mb-2">
        <p className='label'>{label}</p>
        <select required className="form-select mt-2" {...rest}>
            <option value="" disabled selected hidden>Pilih Kategori</option>
            <option value={1}>Accessories</option>
        </select>
    </div>
    )
}

export default Select