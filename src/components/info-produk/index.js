import { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import Input from './Input';
import Select from './Select';
import ButtonPreview from './ButtonPreview';
import ButtonTerbitkan from './ButtonTerbitkan';
import Upload from './Upload';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const client = axios.create({
  baseURL: `https://finalproject-be-kelompok2.herokuapp.com/product/create`,
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token')}`,
    'Content-Type': 'application/json',
  },
  validateStatus: (status) => {
    return true; // I'm always returning true, you may want to do it depending on the status received
  },
});

const InfoProduct = () => {
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [categoryId, setCategoryId] = useState('');
  const [description, setDescription] = useState('');
  const [quantity, setQuantity] = useState('');
  const [image, setImage] = useState({
    myFile: '',
  });
  const [imagePreview, setImagePreview] = useState('input-image.png');
  const [posts, setPosts] = useState([]);

  // Handle form submission
  const handleSubmit = (e) => {
    e.preventDefault();
    addPosts(name, price, categoryId, description, quantity, image);
    console.log('name: ', name);
    console.log('price: ', price);
    console.log('category: ', categoryId);
    console.log('description: ', description);
    console.log('quantity: ', quantity);
    console.log('image: ', image);
  };
  /* POST with Axios */
  const addPosts = async (
    name,
    price,
    categoryId,
    description,
    quantity,
    image
  ) => {
    try {
      let response = await client.post('', {
        name: name,
        price: price,
        categoryId: categoryId,
        description: description,
        quantity: quantity,
        image: image,
      });
      setPosts([response.data, ...posts]);
      setName('');
      setPrice('');
      setCategoryId('');
      setDescription('');
      setQuantity('');
      setImage('');
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const convertToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  const onImageUpload = async (e) => {
    const file = e.target.files[0];
    setImagePreview(URL.createObjectURL(file));
    const base64 = await convertToBase64(file);
    setImage({ ...image, myFile: base64 });
  };

  return (
    <div className="">
      <nav className="navbar navbar-expand-lg nav-info-produk">
        <div class="container-fluid">
          <a
            style={{ fontFamily: 'Poppins' }}
            className="navbar-brand"
            href="#"
          >
            <img
              className="rectangle-navbar"
              style={{ marginRight: 335 }}
              src="rectangle.png"
              width={150}
              height={24}
              alt
            />
          </a>
        </div>
      </nav>
      <div className="form">
        <div className="container mb-2">
          <div className="row">
            <div className="col">
              <a onClick={() => navigate('/daftarjual')}>
                <img className="left-arrow-1" src="left-arrow.png" alt />
              </a>
            </div>
          </div>
        </div>
        <div className="form-seller">
          <Input
            required="true"
            label="Nama Produk"
            name="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Nama Produk"
          ></Input>
          <Input
            label="Harga Produk"
            name="price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            placeholder="Rp 0,00"
          ></Input>
          <Select
            label="Kategori"
            name="categoryId"
            value={categoryId}
            onChange={(e) => setCategoryId(e.target.value)}
          ></Select>
          <Input
            label="Deskripsi"
            name="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Contoh: Jalan Ikan Hiu 33"
            style={{ height: 80, marginBottom: 9 }}
          ></Input>
          <Input
            label="Quantity"
            name="quantity"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
            placeholder="Contoh: 3"
          ></Input>
          <Upload
            label="Foto Produk"
            name="myFile"
            onChange={(e) => onImageUpload(e)}
            img={imagePreview}
          ></Upload>
        </div>
        <br />
        <ButtonPreview
          type="submit"
          title="Preview" /*onClick={() => navigate("/seller-product/:id")}*/
        ></ButtonPreview>
        <ButtonTerbitkan
          title="Terbitakan"
          onClick={handleSubmit}
        ></ButtonTerbitkan>
      </div>
    </div>
  );
};

export default InfoProduct;
