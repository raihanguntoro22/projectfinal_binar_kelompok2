import React from 'react';

const Upload = ({img, label, ...rest}) => {
    return (
        <div className="col upload">
            <p className="label">{label}</p>
            <label for="img">{img && <img for="img" className='input-image-produk' src={img} alt="preview" />}</label>
            <input required accept=".jpeg, .png, .jpg" id="img" className='input-image' type="file" {...rest}/>
        </div>
    )
}

export default Upload