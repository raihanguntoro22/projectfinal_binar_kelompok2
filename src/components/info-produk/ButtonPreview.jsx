import React from 'react';

const ButtonPreview = ({title, ...rest}) => {
    return (
        <button type="submit" className="btn btn-preview mb-4 button-purple" {...rest}>{title}</button>
    )
}

export default ButtonPreview