import React from 'react';

const ButtonTerbitkan = ({title, ...rest}) => {
    return (
        <button type="submit" className="btn btn-terbitkan mb-4 button-purple" {...rest}>{title}</button>
    )
}

export default ButtonTerbitkan