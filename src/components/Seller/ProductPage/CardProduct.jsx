import CardUser from './CardUser';
import { Link, useNavigate } from 'react-router-dom';

function CardProduct({ detailProduct, detailUser }) {
  const navigate = useNavigate();
  return (
    <div>
      <div>
        <div className="card-body">
          <div className="card  border-radius">
            <div className="card-body">
              <h5 className="card-title">{detailProduct.name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {detailProduct.categoryId}
              </h6>
              <p className="card-text">{detailProduct.price}</p>

              <a
                href="#"
                className="card-link btn btn-terbit w-100 mb-3"
                onClick={() => navigate('/daftarjual')}
              >
                Terbitkan
              </a>
              <Link to={`/info-produk/${detailProduct.id}`}>
                <a href="#" className="card-link btn btn-edit w-100">
                  Edit
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <CardUser detailUser={detailUser} />
    </div>
  );
}
export default CardProduct;
