import axios from 'axios';
import { useEffect, useState } from 'react';
import CarouselProduct from './CarouselProduct';
import NavbarProduct from './NavbarProduct';
import { useParams } from 'react-router-dom';

function ProductPageSeller() {
  let { id } = useParams();

  const [detailProduct, setDetailProduct] = useState([]);
  const [detailUser, setDetailUser] = useState([]);
  const getProductsData = async () => {
    try {
      const response = await axios.get(
        `http://localhost:3500/product/details/${id}`
      );
      setDetailProduct(response.data.data);
      const response2 = await axios.post(
        `https://finalproject-be-kelompok2.herokuapp.com/user/by-user`,
        {
          id: response.data.data.userId,
        }
      );
      setDetailUser(response2.data.data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => getProductsData, []);
  return (
    <div>
      <NavbarProduct />
      <CarouselProduct detailProduct={detailProduct} detailUser={detailUser} />
    </div>
  );
}
export default ProductPageSeller;
