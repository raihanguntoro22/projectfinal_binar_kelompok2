import { UncontrolledCarousel } from 'reactstrap';
import CardProduct from './CardProduct';

function CarouselProduct({ detailProduct, detailUser }) {
  return (
    <div className="container container-carousel-parent">
      <div className="d-flex container-carousel">
        <UncontrolledCarousel
          className="carousel-seller-product"
          items={[
            {
              key: 1,
              src: 'https://picsum.photos/id/123/1200/600',
            },
            {
              key: 2,
              src: 'https://picsum.photos/id/456/1200/600',
            },
            {
              key: 3,
              src: 'https://picsum.photos/id/678/1200/600',
            },
          ]}
        />
        <div className="card-right ">
          <CardProduct detailProduct={detailProduct} detailUser={detailUser} />
        </div>
      </div>
      <div className="card card-deskripsi border-radius">
        <div className="card-body">
          <h5 className="card-title">Deskripsi</h5>
          <p className="card-text">{detailProduct.description}</p>
        </div>
      </div>
    </div>
  );
}

export default CarouselProduct;
