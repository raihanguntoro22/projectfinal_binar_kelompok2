import { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Navbar,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  Form,
  Input,
  NavLink,
} from 'reactstrap';
import { AiOutlineUnorderedList } from 'react-icons/ai';
import { FiUser } from 'react-icons/fi';
import { IoIosNotificationsOutline } from 'react-icons/io';
import { Link } from 'react-router-dom';

function NavbarProduct() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <Navbar
        expand="lg"
        light
        container
        className="shadow-sm mb-3 navbar-seller"
      >
        <Link className="navbarBrand" to="/"></Link>
        <NavbarToggler
          onClick={() => {
            setIsOpen(!isOpen);
          }}
        />
        <Collapse isOpen={isOpen} navbar>
          <Form className="me-auto">
            <Input
              id="exampleSearch"
              name="search"
              placeholder="Cari di sini...."
              type="search"
            />
          </Form>
          <Nav className="ms-auto" navbar>
            <NavItem>
              <NavLink href="/">
                <AiOutlineUnorderedList />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/">
                <IoIosNotificationsOutline />
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/">
                <FiUser />
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavbarProduct;
