import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
function CarouselHome() {
  const settings = {
    arrows: false,
    infinite: true,
    // autoplay: true,
    speed: 1000,
    // autoplaySpeed: 4000,
    centerMode: true,
    draggable: true,
    centerPadding: '300px',
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      { breakpoint: 500, settings: { autoplay: true, slidesToShow: 1 } },
    ],
  };
  return (
    <div>
      <div className="img-banner-responsive"></div>
      <Slider {...settings} className="carousel-home">
        <img
          src="img/img-banner.png"
          alt=""
          className="img-banner border-radius"
          style={{ objectFit: 'cover' }}
        />
        <img
          src="img/img-banner.png"
          alt=""
          style={{ objectFit: 'cover' }}
          className="img-banner border-radius"
        />
        <img
          src="img/img-banner.png"
          alt=""
          style={{ objectFit: 'cover' }}
          className="img-banner border-radius"
        />
      </Slider>
    </div>
  );
}
export default CarouselHome;
