import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Navbar,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  Input,
} from 'reactstrap';
import { MdLogin } from 'react-icons/md';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { FiUser } from 'react-icons/fi';
import { AiOutlineUnorderedList } from 'react-icons/ai';
import { IoIosNotificationsOutline } from 'react-icons/io';
import CardProduct from './CardProduct';
import SimpleSlider from './SimpleSlider ';

function NavbarHome() {
  const [isOpen, setIsOpen] = useState(false);
  const [refetch, setRefetch] = useState([]);
  const [isLoggedin, setIsLoggedin] = useState(false);
  const [modalLogout, setModalLogout] = useState(false);

  const currentUser = async () => {
    try {
      const response = await axios.get(
        'https://finalproject-be-kelompok2.herokuapp.com/user/current-user',
        {
          headers: { authorization: localStorage.getItem('token') },
        }
      );
      setRefetch(response.data.data);
      setIsLoggedin(true);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => currentUser, []);

  const nav = useNavigate();
  const logout = () => {
    localStorage.removeItem('token');
    setIsLoggedin(false);
    nav('/login');
  };

  function handleModalLogout() {
    setModalLogout(!modalLogout);
  }

  const [products, setProduct] = useState([]);
  const [filteredProduct, setFilteredProduct] = useState([]);

  const getProductsData = async () => {
    try {
      const response = await axios.get('https://finalproject-be-kelompok2.herokuapp.com/product/list');
      setProduct(response.data.data);
      setFilteredProduct(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const [searchInput, setSearchInput] = useState('');
  const filterResult = (productParam) => {
    setFilteredProduct(
      products.filter((product) => product.categoryId === productParam)
    );
    setSearchInput(productParam);
    if (searchInput !== '') {
      const filteredData = products.filter((item) => {
        return Object.values(item)
          .join('')
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredProduct(filteredData);
    } else {
      setFilteredProduct(products);
    }
  };
  useEffect(() => getProductsData, []);
  return (
    <div>
      <Navbar className="navbar-homepage " expand="lg" light container>
        <Link className="navbarBrand navbarBrand-homepage" to="/"></Link>
        <NavbarToggler
          onClick={() => {
            setIsOpen(!isOpen);
          }}
          className="me-auto border-radius"
        />
        <Input
          id="exampleSearch"
          name="search"
          placeholder="Cari di sini...."
          type="search"
          onChange={(e) => filterResult(e.target.value)}
          className="search-navbar border-radius"
        />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ms-auto" navbar>
            <NavItem>
              {isLoggedin ? (
                <Nav className="ms-auto" navbar>
                  <NavItem>
                    <NavLink href="/daftarjual">
                      <AiOutlineUnorderedList />
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#" id="PopoverClick" type="button">
                      <IoIosNotificationsOutline />
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink>
                      <Link to="/info-profil">
                        <FiUser />
                      </Link>
                    </NavLink>
                  </NavItem>
                  <Link
                    to="/login"
                    className="btn btn-purple border-radius ms-2 btn-logout"
                    onClick={logout}
                  >
                    <MdLogin />
                  </Link>
                </Nav>
              ) : (
                <Link to="/login" className="btn btn-purple border-radius p-2">
                  <MdLogin /> Masuk
                </Link>
              )}
            </NavItem>
          </Nav>
        </Collapse>
        {/* {modalLogout && (
          <div
            className="card-atas position-absolute  "
            style={{
              width: '100px',
              height: '50px',
              right: '70px',
              top: '60px',
            }}
            onClick={logout}
          >
            <p className="text-center logout-pointer">
              <b>Logout</b>
            </p>
          </div>
        )} */}
      </Navbar>
      <SimpleSlider />
      <CardProduct
        products={products}
        filteredProduct={filteredProduct}
        setFilteredProduct={setFilteredProduct}
        filterResult={filterResult}
      />
    </div>
  );
}

export default NavbarHome;
