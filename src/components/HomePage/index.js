import ButtonHome from './ButtonHome';
import NavbarHome from './NavbarHome';
function HomePage() {
  return (
    <div>
      <NavbarHome />
      <ButtonHome />
    </div>
  );
}
export default HomePage;
