import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import AkunPenawarMenu from "./AkunPenawar";
import ListPenawarMenu from "./ListPenawar";
import NavbarPenawarMenu from "./NavbarPenawar";


function InfoPenawarMenu() {
  const [products, setProducts] = useState([]);
  const [idUser, setIdUser] = useState(); 

  useEffect(() => {

      const fetchData = async () => {
          try {
              // Check status user login
              // 1. Get token from localStorage
              const token = localStorage.getItem("token");

              // 2. Check token validity from API
              const currentUserRequest = await axios.get(
                  `https://finalproject-be-kelompok2.herokuapp.com/offer/list`,
                  {
                      headers: {
                          Authorization: `Bearer ${token}`,
                      },
                  }
              ).then((item) => {
                setProducts(item.data.data)
                setIdUser(item.data.data[0].userId)
              })
          } catch (err) {
              console.log(err);
          }
      };


      fetchData();
  }, []);
  

    return (
      <div>
        <NavbarPenawarMenu/>
        <AkunPenawarMenu  idUser={idUser}/>
        <ListPenawarMenu products={products}/>
      </div>
    );
  }
  
  export default InfoPenawarMenu;
