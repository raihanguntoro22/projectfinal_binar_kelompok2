import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card, CardBody, CardSubtitle, CardTitle } from "reactstrap";
import { Link } from "react-router-dom";
import { BiArrowBack } from "react-icons/bi";


const AkunPenawarMenu = ({ idUser }) => {
    const [users, setUsers] = useState([]);


    useEffect(() => {

        const fetchData = async () => {
            console.log(idUser)
            if (idUser) {
                try {
                    // Check status user login
                    // 1. Get token from localStorage
                    const token = localStorage.getItem("token");
                    const id = idUser ? idUser : ""
                    // 2. Check token validity from API
                    const currentUserRequest = await axios(

                        {
                            method: "post",
                            data: { id },
                            url: "https://finalproject-be-kelompok2.herokuapp.com/user/by-user",
                            headers: {
                                Authorization: `Bearer ${token}`,
                            },
                        }
                    ).then((item) => setUsers(item.data.data))
                } catch (err) {
                    console.log(err);
                }
            }

        };

        fetchData();
    }, [idUser]);

    return (
        <Card className="mx-25 mt-3">
            <div className="d-flex">
                <Link to="/daftarjual"><BiArrowBack style={{ marginRight: "60px", fontSize: "20px" }} /></Link>
                <CardBody>
                    <div className="d-flex " style={{ justifyContent: "space-between", height: "50px" }}>
                        <div className="d-flex ">
                            <img src={users.image} alt="" width="50px" />
                            <div className="ms-2">
                                <CardTitle tag="h5">
                                    {users.name}
                                </CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h7">
                                    {users.city}
                                </CardSubtitle>
                            </div>
                        </div>
                    </div>
                </CardBody>
            </div>
        </Card>
    )
}
export default AkunPenawarMenu;