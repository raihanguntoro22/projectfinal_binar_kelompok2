import React from "react";
import { Navbar, NavbarBrand} from "reactstrap";


const NavbarPenawarMenu = () => {


    return (
        <div className="" >
            <div>
                <Navbar
                    color="light"
                    expand="md"
                    light
                    className=""


                >
                    <NavbarBrand href="/">
                        <img src="img/logo.png" alt="" />
                    </NavbarBrand>
                    <h5 style={{margin: "auto"}}>Info Penawaran</h5>
                </Navbar>
            </div>
        </div >
    )
}
export default NavbarPenawarMenu;