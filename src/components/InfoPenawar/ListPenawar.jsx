import React, { useEffect, useState } from "react";
import axios from "axios";
import { Button, Card, CardBody, CardSubtitle, CardText, CardTitle } from "reactstrap";
import { Modal } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { BsWhatsapp} from "react-icons/bs";



const ListPenawarMenu = ({ products }) => {
    const { id } = useParams();
    const filtered = Object.keys(products).length !== 0 ? products.filter((item) => item.id === Number(id)) : ''
    

    const [modal, setmodal] = useState(false)
    // const [status, setstatus] = useState(false)
    // const [statusInputRadio, setstatusInputRadio] = useState("")

    const [solds, setSolds] = useState([]);
    const filterStatus = Object.keys(solds).length !== 0 ? solds.filter((item) => item.offerId === Number(id)) : ''
  

    const handleOpen = () => {
        setmodal(true)

    }
    const handleClose = async () => {
        setmodal(false)
        try {
            // Check status user login
            // 1. Get token from localStorage
            const token = localStorage.getItem("token");

            const sold2 = { offerId: `${id}` }

            // 2. Check token validity from API

            const currentUserRequest22 = await axios.post(
                "https://finalproject-be-kelompok2.herokuapp.com/order/create", sold2,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            )
        } catch (err) {
            console.log(err);
        }
    } 

    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserSell = await axios.get(
                    "https://finalproject-be-kelompok2.herokuapp.com/order/list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setSolds(item.data.data))
            } catch (err) {
                console.log(err);
            }

        };

        fetchData();

    }, [solds]);
    console.log(solds);

    return (
        <div>
            <Card className="mx-25 mt-3 bg-gray br-10">

                {filtered !== undefined && Object.keys(filtered).length !== 0 ?
                    <CardBody className=" ">
                        <div className="d-flex " style={{ height: "100px" }}>
                            <div className="d-flex" sty>
                                <img src={filtered[0].Product.image} alt="" width="50%" className="me-3" />
                                <div className="ms-2" style={{ width: "100%" }}>
                                    {filtered[0].Product.description}
                                    <CardTitle tag="h5">
                                        {filtered[0].Product.name}
                                    </CardTitle>
                                    <CardText tag="h6">
                                        
                                        Ditawar {filtered[0].price}
                                    </CardText>
                                </div>
                            </div>
                        </div>
                    </CardBody>

                    : ""}
                <div className="mt-3" style={{ justifyContent: "flex-end", display: Object.keys(filterStatus).length !==0 ? filterStatus[0].status == "accepted" ? "none" : "flex" :"flex"  }}>
                    <Button style={{ margin: "end" }} className="btn-tawar mt-4 me-2 h6"><h6>Tolak</h6></Button>
                    <Button onClick={handleOpen} className="btn-tawar mt-4 h6"><h6>Terima</h6></Button>
                </div>
            </Card>

            <Modal show={modal} className="modalb">
                <div >
                    <Modal.Header closeButton onClick={handleClose}>
                    </Modal.Header>
                    <Modal.Body >
                        <Card>
                            <h6> <b>Yeay kamu berhasil mendapatkan harga yang sesuai</b></h6>
                            <h6 className="text-muted mt-0">Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya</h6>
                        </Card>
                        <Card >
                            <CardBody className="bg-gray br-10 mt-2">
                                {filtered !== undefined && Object.keys(filtered).length !== 0 ?
                                    <Link to="#">
                                        <div className="d-flex mb-4 " style={{ justifyContent: "space-between", height: "60px" }}>
                                            <div className="d-flex ">
                                                <img src={filtered[0].Product.image} alt="" width="100px" />
                                                <div className="ms-2">
                                                    <CardSubtitle className="mb-2 text-muted" tag="h7">
                                                        {filtered[0].Product.description}
                                                    </CardSubtitle>
                                                    <div>
                                                        <CardTitle tag="h6">
                                                            {filtered[0].Product.name}
                                                        </CardTitle>
                                                        <CardText tag="h6">
                                                            <div className="mb-1"> {filtered[0].status}</div>
                                                            Ditawar {filtered[0].price}
                                                        </CardText>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                    : ""}
                            </CardBody>
                        </Card>
                        <div style={{ justifyContent: "space-between" }} className="ms-3"  >
                            <Button onClick={handleClose} className="btn-modal mt-4 me-2 "><h6>Selesai</h6> </Button>
                            <Button href="https://wa.me/6285735501035?" className="btn-modal mt-4 "><h6> Hubungi di <BsWhatsapp className="ms-1" /></h6></Button>
                        </div>
                    </Modal.Body>
                </div>
            </Modal>
            <div>
                {/* <Modal show={status} className="modalb">
                    <div >
                        <Modal.Header className="h6 ms-5" closeButton onClick={statusClose}>
                            Perbarui status penjualan produkmu
                        </Modal.Header>
                        <Modal.Body >
                            <Card>

                            </Card>
                            <Card >
                                <CardBody className="bg-gray br-10" >
                                    <Form onSubmit={handleOpen}>
                                        <div className=" d-flex"  >
                                            <input type="radio" onChange={() => setstatusInputRadio("accepted")} id="status1" name="status" className="mt-1" />
                                            <div className="ms-2">
                                                <label htmlFor="status1"><h6> Berhasil terjual</h6></label>
                                                <label htmlFor="status1" className="text-muted"><h6 >Kamu telah sepakat menjual produk ini kepada pembeli</h6></label>
                                            </div>
                                        </div>
                                        <div className=" d-flex mt-3">
                                            <input type="radio" onChange={() => setstatusInputRadio("rejected")} id="status2" name="status" className="mt-1" />
                                            <div className="ms-2">
                                                <label htmlFor="status2"> <h6>Batalkan transaksi</h6></label>
                                                <label htmlFor="status2"> <h6 className="text-muted">Kamu membatalkan transaksi produk ini dengan pembeli</h6></label>
                                            </div>
                                        </div>
                                    </Form>
                                </CardBody>
                            </Card>
                            <div style={{ justifyContent: "space-between" }} className="ms-3" >
                                <Button onClick={sendOrder} className="btn-modal3 mt-4 me-2 "><h6>Kirim</h6> </Button>
                            </div>
                        </Modal.Body>
                    </div>
                </Modal> */}
            </div>

        </div>
    )
}
export default ListPenawarMenu;