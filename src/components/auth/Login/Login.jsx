import React, { useRef, useState } from "react";
import axios from "axios";
import {  FormGroup, Label,  Button } from "reactstrap";
import { FiEye } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

export const LoginMenu = () => {
    const navigate = useNavigate();

  const emailField = useRef("");
  const passwordField = useRef("");

  const [errorResponse, setErrorResponse] = useState({
    isError: false,
    message: "",
  });

  const onLogin = async (e) => {
    e.preventDefault();

    try {
      const userToLoginPayload = {
        email: emailField.current.value,
        password: passwordField.current.value,
      };

      const loginRequest = await axios.post(
        "https://finalproject-be-kelompok2.herokuapp.com/user/login",
        userToLoginPayload
      );

      const loginResponse = loginRequest.data;

      if (loginResponse.success) {
        localStorage.setItem("token", loginResponse.data.token);
        navigate("/");
      }
    } catch (err) {
      console.log(err);
      const response = err.response.data;

      setErrorResponse({
        isError: true,
        message: response.message,
      });
    }
  };
    const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        // When the handler is invoked
        // inverse the boolean state of passwordShown
        setPasswordShown(!passwordShown);

    };
    
    return (
        <div>
            <div className="row">
                <div className="col-md-6">
                    <img src="img/login.png" alt="" style={{ width: '95%', height: '700px', objectFit:"cover" }} className="none" />
                </div>
                <div className="col-md-6">
                    <div>
                        <div className="container">
                            <div className="px-5 up-20" >
                                <h3><b>Masuk</b></h3>
                                <form onSubmit={onLogin}>
                                    <FormGroup className="mt-3 mb-2 me-sm-2 mb-sm-0">
                                        <Label
                                            className="mt-3 "
                                            for="exampleEmail"
                                        >
                                            Email
                                        </Label>
                                        <input

                                            placeholder="Contoh: johndee@gmail.com"
                                            ref={emailField}
                                            className="input-form"
                                        />
                                    </FormGroup>
                                    <FormGroup className="mt-2 mb-2 me-sm-2 mb-sm-0">
                                        <Label
                                            className=" mt-3"
                                            for="examplePassword"
                                        >
                                            Password
                                        </Label>
                                        <div className="d-flex">
                                            <input
                                                ref={passwordField}
                                                placeholder="Masukkan password"
                                                type={passwordShown ? "text" : "password"}
                                                className="input-form"

                                            />
                                            <Button type="button" className=" icon-pass" onClick={togglePassword}><FiEye/></Button>

                                        </div>

                                    </FormGroup>
                                    <Button type="submit" className="mt-4 btn-success border-radius-15">
                                        Masuk
                                    </Button>
                                    <p className="text-center mt-3">Belum punya akun?   <a href="/register" className="fw-bold">Daftar disini</a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}
export default LoginMenu;
