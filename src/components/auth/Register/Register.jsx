import axios from "axios";
import React, { useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import { FiEye } from "react-icons/fi";



export const RegisterMenu = () => {
    const navigate = useNavigate();

    const nameField = useRef("");
    const emailField = useRef("");
    const passwordField = useRef("");

    const onRegister = async (e) => {
        e.preventDefault();

        try {
            const userToRegisterPayload = {
                name: nameField.current.value,
                email: emailField.current.value,
                password: passwordField.current.value,
            };

            const registerRequest = await axios.post(
                "https://finalproject-be-kelompok2.herokuapp.com/user/register",
                userToRegisterPayload
            );

            const registerResponse = registerRequest.data;

            if (registerResponse.success) navigate("/login");
        } catch (err) {
            console.log(err);
        }
    };

    const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        // When the handler is invoked
        // inverse the boolean state of passwordShown
        setPasswordShown(!passwordShown);
    };
    return (
        <div>
            <div className="row">
                <div className="col-md-6">
                    <img src="img/login.png" alt="" style={{ width: '95%', height: '700px' }} className="none" />
                </div>
                <div className="col-md-6">
                    <div>
                        <div className="container ">
                            <div className="px-5 up-20" >
                                <h3><b>Daftar</b></h3>
                                <Form inline onSubmit={onRegister}>
                                    <FormGroup className="mt-3 mb-2 me-sm-2 mb-sm-0">
                                        <Label
                                            className="mt-3 me-sm-2"
                                            for=""
                                        >
                                            Nama
                                        </Label>
                                       
                                            <input
                                                ref={nameField}
                                                placeholder="Nama Lengkap"
                                                className="input-form"
                                            />

                                    </FormGroup>
                                    <FormGroup className="mt-2 mb-2 me-sm-2 mb-sm-0">
                                        <Label
                                            className="mt-3 me-sm-2"
                                            for="exampleEmail"
                                        >
                                            Email
                                        </Label>

                                        <input
                                            ref={emailField}
                                            placeholder="Contoh: johndee@gmail.com"
                                            type="email"
                                            className="input-form"
                                        />


                                    </FormGroup>
                                    <FormGroup className="mt-2 mb-2 me-sm-2 mb-sm-0">
                                        <Label
                                            className=" mt-3 me-sm-2"
                                            for="examplePassword"
                                        >
                                            Password
                                        </Label>
                                        <div className="d-flex">
                                            <input
                                                ref={passwordField}
                                                placeholder="Masukkan password"
                                                type={passwordShown ? "text" : "password"}
                                                className="input-form"


                                            />
                                            <Button className=" icon-pass" onClick={togglePassword}><FiEye /></Button>
                                        </div>

                                    </FormGroup>
                                    <Button type="submit" className="mt-4 btn-success border-radius-15">
                                        Daftar
                                    </Button>
                                    <p className="text-center mt-3">Sudah punya akun? <Link to="/login" className="fw-bold" >Masuk disini</Link></p>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}
export default RegisterMenu;
