import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, Col, Row } from "reactstrap";
import { Link } from "react-router-dom";
import { BsPlusLg } from "react-icons/bs";



const ListProduk = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserRequest = await axios.get(
                    "https://finalproject-be-kelompok2.herokuapp.com/product/sale-list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setProducts(item.data.data))
            } catch (err) {
                console.log(err);
            }
        };

        fetchData();
    }, []);
    console.log(products);
    return (
        <Col md={8}>
            <Row>
                <Col md={4} className="mb-2 width-50">
                    <Card className="text-center">

                        <Link to="/info-produk">
                            <CardImg
                                alt=""
                                src=""
                                top
                                width="100%"
                                className=""
                            />
                            <CardBody className="mt-5">

                                <CardTitle tag="h5" className="text-muted">
                                    <BsPlusLg />
                                </CardTitle>
                                <CardSubtitle
                                    className="mb-5 text-muted"
                                    tag="h5"
                                >
                                    Tambah Produk
                                </CardSubtitle>

                            </CardBody>
                        </Link>
                    </Card>
                </Col>
                {products.map((product) => (

                    <Col md={4} className="mb-2 width-50">
                        <Link to={`/seller-product/${product.id}`}>
                            <Card key={product.id}>
                                <CardImg
                                    alt="Card image cap"
                                    src={product.image}
                                    top
                                    width="100%"
                                />
                                <CardBody>
                                    <CardTitle tag="h6">
                                        {product.name}
                                    </CardTitle>
                                    <CardSubtitle
                                        className="mb-2 text-muted"
                                        tag="p"
                                    >
                                        {product.description}
                                    </CardSubtitle>
                                    <CardText tag="h6">
                                        {product.price}
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Link>
                    </Col>

                ))}
            </Row>
        </Col>
    )
}
export default ListProduk