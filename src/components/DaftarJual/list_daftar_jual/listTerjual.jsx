import React, { useEffect, useState } from "react";
import axios from "axios";
import {  Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, Col, Row } from "reactstrap";
import { Link } from "react-router-dom";


const ListTerjual = () => {
    const [solds, setSolds] = useState([]);

    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserSell = await axios.get(
                    "https://finalproject-be-kelompok2.herokuapp.com/order/list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setSolds(item.data.data))
            } catch (err) {
                console.log(err);
            }
        };

        fetchData();
    }, []);
    console.log(solds);
    return (
        <Col md={8}>
            <Row>
                {solds.map((solds) => (
                    <Col md={4} className="mb-2 width-50">
                        <Link to={`/info-penawar/${solds.id}`}>
                            <Card key={solds.Offer.Product.id}>
                                <CardImg
                                    alt="Card image cap"
                                    src={solds.Offer.Product.image}
                                    top
                                    width="100%"
                                />
                                <CardBody>
                                    <CardTitle tag="h6">
                                        {solds.Offer.Product.name}
                                    </CardTitle>
                                    <CardSubtitle
                                        className="mb-2 text-muted"
                                        tag="p"
                                    >
                                        {solds.Offer.Product.description}
                                    </CardSubtitle>
                                    <CardText tag="h6">
                                        {solds.Offer.Product.price}
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Link>
                    </Col>

                ))}
            </Row>
        </Col>
    )
}
export default ListTerjual