import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle, Col, Row } from "reactstrap";
import { Link, useParams } from "react-router-dom";


const ListDiminati = () => {
    const [orders, setOrders] = useState([]);
    const [orders2, setOrders2] = useState([]);
    const { id } = useParams();

    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserOrder = await axios.get(
                    "https://finalproject-be-kelompok2.herokuapp.com/offer/list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setOrders(item.data.data))
            } catch (err) {
                console.log(err);
            }
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserOrder = await axios.get(
                    `https://finalproject-be-kelompok2.herokuapp.com/order/${id}`,
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setOrders2(item.data.data))
            } catch (err) {
                console.log(err);
            }
            
        };

        fetchData();
    }, []);
    console.log(orders);
    return (
        <Col md={8}>
            <Row>
                {orders.map((orders, orders2) => (
                    <Col md={4} className="mb-2 width-50">
                        <Link to={`/info-penawar/${orders.id}`}>
                            <Card key={orders.Product.id}>
                                <CardImg
                                    alt="Card image cap"
                                    src={orders.Product.image}
                                    top
                                    width="100%"
                                />
                                <CardBody>
                                    <CardTitle tag="h6">
                                        {orders.Product.name}
                                    </CardTitle>
                                    <CardSubtitle
                                        className="mb-2 text-muted"
                                        tag="p"
                                    >
                                        {orders.Product.description}
                                        {orders2.status}
                                        
                                    </CardSubtitle>
                                    <CardText tag="h6">
                                        <div className="mb-1">Rp {orders.Product.price}</div>
                                       Ditawar Rp {orders.price}
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Link>
                    </Col>

                ))}
            </Row>
        </Col>
    )
}
export default ListDiminati