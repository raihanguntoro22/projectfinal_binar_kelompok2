import React, { useEffect, useState } from "react";
import axios from "axios";
import { BsBell, BsListUl } from "react-icons/bs";
import { BiUser } from "react-icons/bi";
import { Card, CardBody, CardSubtitle, CardTitle, Collapse, Input, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem } from "reactstrap";
import { Modal } from "react-bootstrap";
import { Link } from "react-router-dom";


const NavbarMenu = () => {

    const [modal, setmodal] = useState(false)
    const handleOpen = () => {
        setmodal(true)
    }
    const handleClose = () => {
        setmodal(false)
    }

    const [order, setOrders] = useState([]);
    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserOrder = await axios.get(
                    "https://finalproject-be-kelompok2.herokuapp.com/offer/list",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setOrders(item.data.data))
            } catch (err) {
                console.log(err);
            }
            
        };

        fetchData();
    }, []);
  
    return (
        <div className="none" >
            <div  >
                <Navbar
                    color="light"
                    expand="md"
                    light
                    className=""


                >
                    <NavbarBrand href="/">
                        <img src="img/logo.png" alt="" />
                    </NavbarBrand>
                    <NavItem className="d-flex">
                        <Input
                            className="form-input-atas"
                            id=""
                            placeholder="cari di sini ..."
                            type="text"
                        />

                    </NavItem>
                    <NavbarToggler
                        className="me-2"
                        onClick={function noRefCheck() { }}
                    />
                    <Collapse navbar style={{ justifyContent: "end" }}>
                        <Nav navbar >
                            <NavItem>
                                <Link to="/daftarjual">
                                    <BsListUl style={{ fontSize: "20px" }} className="purplee" />
                                </Link>
                            </NavItem>
                            <NavItem onClick={handleOpen}>

                                <BsBell style={{ fontSize: "20px", marginLeft: "20px", cursor: "pointer" }} className="purplee" />

                            </NavItem>
                            <NavItem>
                                <Link to="/info-profil">
                                    <BiUser style={{ fontSize: "20px", marginLeft: "20px" }} className="purplee" />
                                </Link>
                            </NavItem>
                        </Nav>
                    </Collapse>

                    <Modal show={modal} className="modala " >
                        <Modal.Header onClick={handleClose} closeButton>
                            <Modal.Title>Notification</Modal.Title>
                        </Modal.Header>
                        <Modal.Body className="modal-backdrop.showa">
                            <Card >
                                <CardBody>
                                    {order.map((product) => (
                                        <Link to="#">
                                            <div className="d-flex mb-4 " style={{ justifyContent: "space-between", height: "60px" }}>
                                                <div className="d-flex ">
                                                    <img src={product.Product.image} alt="" width="100px" />
                                                    <div className="ms-2">
                                                        <CardSubtitle className="mb-2 text-muted" tag="h7">
                                                            {product.Product.description}
                                                        </CardSubtitle>
                                                        <div>
                                                            <CardTitle tag="h6">
                                                                {product.Product.name}
                                                            </CardTitle>
                                                            <CardTitle tag="h6">
                                                                {product.Product.price}
                                                            </CardTitle>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    ))}
                                </CardBody>
                            </Card>
                        </Modal.Body>
                    </Modal>
                </Navbar>
            </div >
        </div >
    )
}
export default NavbarMenu;