import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Card, CardBody, CardSubtitle, CardTitle} from "reactstrap";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { useSelector } from "react-redux";


const CategoryMenu = () => {
    const alert = useSelector(state => state.product.alert);
    const [show, setShow] = useState(true);

    const handleClose = () => { setShow(false) }

    const [users, setUsers] = useState([]);
    useEffect(() => {

        const fetchData = async () => {
            try {
                // Check status user login
                // 1. Get token from localStorage
                const token = localStorage.getItem("token");

                // 2. Check token validity from API
                const currentUserRequest = await axios.get(
                    "https://finalproject-be-kelompok2.herokuapp.com/user/current-user",
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                ).then((item) => setUsers(item.data.data))
            } catch (err) {
                console.log(err);
            }
        };

        fetchData();
    }, []);
    console.log(users);


    return (
        <div className="container mt-4">
            <div className="mx-60">

                <Stack sx={{ width: "50%", left: '27%', right: 0, top: 0, transition: '0.5s', marginTop: show ? { xs: "120px", md: '50px' } : "-350px", position: 'absolute', display: alert ? 'block' : 'none' }} spacing={2}>
                    <Alert onClose={handleClose}>{alert}</Alert>
                </Stack>

                <div className="mx-60">
                    <h4><b>Daftar Jual Saya</b></h4>
                </div>
                <div className=" mx-60">
                    <Card>
                        <CardBody>
                            <div className="d-flex " style={{ justifyContent: "space-between", height: "50px" }}>
                                <div className="d-flex ">
                                    <img src={users.image} alt="" width="50px" />
                                    <div className="ms-2">
                                        <CardTitle tag="h5">
                                            {users.name}
                                        </CardTitle>
                                        <CardSubtitle className="mb-2 text-muted" tag="h7">
                                            {users.city}
                                        </CardSubtitle>
                                    </div>
                                </div>
                                <Link to="/info-profil">
                                    <button className="mt-2 btn-success-edit">Edit</button>
                                </Link>
                            </div>
                        </CardBody>
                    </Card>
                </div>
            </div>
        </div>
    )
}
export default CategoryMenu