import React from "react";
import { BsBox, BsChevronRight, BsCurrencyDollar, BsHeart } from "react-icons/bs";
import { Button, Card, CardBody } from "reactstrap";


const ButtonDaftarJual = ({ setList, clicked }) => {

    return (
        <div className="col-md-3 border-none">
            <Card>
                <CardBody>
                    <div className="ds-none" >
                        <Button
                            onClick={() => setList("produk")}
                            className="btn-category me-2"
                           
                        >
                            <h6><BsBox style={{ marginRight: "10px" }} />Produk</h6>
                        </Button>
                        <Button
                            onClick={() => setList("diminati")}
                            className="btn-category me-2"
                        >
                            <h6><BsHeart style={{ marginRight: "10px" }} />Diminati</h6>
                        </Button>
                        <Button
                            onClick={() => setList("terjual")}
                            className="btn-category"
                        >
                            <h6><BsCurrencyDollar style={{ marginRight: "10px" }} />Terjual</h6>
                        </Button>
                    </div>
                    <div className="none">
                        <h5>Kategori</h5>


                        <div className="mt-4 d-flex purplee"  onClick={() => setList("produk")} style={{ justifyContent: "space-between", cursor: "pointer", color: clicked=== "produk"? "#7126b5" : "" }}>
                            <h6 ><BsBox style={{ marginRight: "10px" }} />Semua produk</h6>
                            <h6><BsChevronRight /></h6>
                        </div>


                        <hr />

                        <div className="mt-2 d-flex purplee" onClick={() => setList("diminati")} style={{ justifyContent: "space-between", cursor: "pointer", color: clicked=== "diminati"? "#7126b5" : ""  }}>
                            <h6><BsHeart style={{ marginRight: "10px" }} />Diminati </h6>
                            <h6><BsChevronRight /></h6>
                        </div>

                        <hr />

                        <div className="mt-2 d-flex purplee" onClick={() => setList("terjual")} style={{ justifyContent: "space-between", cursor: "pointer", color: clicked=== "terjual"? "#7126b5" : ""  }}>
                            <h6><BsCurrencyDollar style={{ marginRight: "10px" }} />Terjual </h6>
                            <h6><BsChevronRight /></h6>
                        </div>

                    </div>

                </CardBody>
            </Card>
        </div>

    )
}
export default ButtonDaftarJual
